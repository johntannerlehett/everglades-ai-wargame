# Everglades AI - UCF Black Team

## Evaluating

There are 2 primary scripts used in evaluations: `evaluate.py` and `evaluate_all.py`.

`evaluate.py` will evaluate a saved agent (RL or state machine) against another specified
agent. In order to do change which agent is loaded for each side, you
must edit the code directly.

There are three variables that can be modified in this file: `PLAYER_1_AGENT`,
`PLAYER_2_AGENT`, and `RENDER_EVERY_N_EPISODES`. To set either `PLAYER_1_AGENT` or
`PLAYER_2_AGENT`, you must first choose whether you want to load a state machine agent
or a saved RL agent.

To load a state machine agent, utilize the `loadStateMachine` function, and pass
in a `selection` string from the list of `STATE_MACHINE_OPTIONS` defined in that script.
You must also pass in the `player_num` as a parameter.

To load a saved RL agent, utilize the `AgentLoader.loadAgent` function, and pass in
the `save_file_path` as a parameter, specifying the path to the saved agent file you
want to use. You must also pass in the `player_num` as a parameter.

The `RENDER_EVERY_N_EPISODES` constant can be modified to state how often to render the
game. The default is set to `1` so every game will be rendered. If you are interested in
obtaining the average win statistics faster, you can opt to set this to a higher value,
or turn it off by setting it to `0`, since the rendering adds additional time to the
computations.

`evaluate_all.py` will take a specified saved agent (RL or state machine), and play it
for `n_episodes` number of episodes against a set of hand-picked `state_machines_to_test`.

You can modify the list of `state_machines_to_test` by adding the selection strings
defined in the `STATE_MACHINE_OPTIONS` variable to the `state_machines_to_test` list.

You must specify the saved agent file to load in the `PLAYER_1_AGENT_SAVE_FILE` variable.
The script assumes the saved agent file is in the `saved-agents` subdirectory, so you just
need to provide the name of the save file (without the extension).

The `SIGNIFICANCE_LEVEL` variable can be changed to determine what significance level to
display for the win rates against each state machine agent in the final evaluation graph.

Once the agent has played `n_episodes` number of games against each state machine agent
in `state_machines_to_test`, a final evaluation graph will be produced. This graph shows
a bar chart of the win rates the saved agent had against each of the state machine agents.
The "I" symbols in the bar chart show the confidence interval for the true win rates
determined by the `SIGNIFICANCE_LEVEL` you set.

## Training

Each of the agents in the `agents` subdirectory have their own separate training scripts.
For example, to train a new "Smart State" agent, you would navigate to the
`agents/Smart_State/training_scripts` directory, and choose the training script you'd like
to run. Each agent has a different selection of training scripts that it supports, so they
won't all be detailed here.

Each training script should have 2-3 variables you can change to modify how the training
will operate, which will appear under the imports for the file. You may also want to change
the `network_save_name` and `network_load_name` parameters on the line in which the
RL agent is instantiated.

## Summary of Trained Agents

The `saved-agents` subdirectory contains all of the agents trained over the course
of our project, and can be loaded in the `evaluate.py` or `evaluate_all.py` script.

`66-93.pickle` is a Smart State agent trained for 500 episodes with a single hidden layer
of 80 nodes.

`70-87_fc2.pickle` is a Smart State agent trained for 500 episodes with 2 hidden layers of
80 nodes each.

`best_smart_state.pickle` is our most well-rounded agent, and is a Smart State agent
trained for 2,000 episodes with 2 hidden layers of 80 nodes each.

`demo0-30.pickle`, `demo0-70.pickle`, `demo1-30.pickle`, and `demo1-70.pickle` were the
saved agents used in the Smart State self play demo. `demo0-30.pickle` was the player 1
Smart State agent trained for only 30 episodes, and `demo0-70.pickle` was the player 1
Smart State agent trained for 70 episodes. `demo1-30.pickle` and `demo1-70.pickle` were
the corresponding player 2 files.

The various `A2C` files are our trained A2C agents.

`ppo-4000.pickle` is our PPO agent trained for 4,000 episodes.

`rppo-5000.pickle` is our Recurrent PPO agent trained for 5,000 episodes.

The rest were a variety of other agents trained throughout the course, and can be loaded
and evaluated as well.
